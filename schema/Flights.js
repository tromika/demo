cube(`Flights`, {
  sql: `SELECT * FROM public.flights`,

  joins: {

  },

  measures: {
    count: {
      type: `count`,
      drillMembers: []
    },

    flightNumber: {
      sql: `${CUBE}."FLIGHT_NUMBER"`,
      type: `sum`
    },
    cancelledCount: {
      type: `count`,
      filters: [
        { sql: `${CUBE}.cancelled = 1` }
      ]
    }
  },

  dimensions: {
    airline: {
      sql: `${CUBE}."AIRLINE"`,
      type: `string`
    },

    cancelled: {
      sql: `${CUBE}."CANCELLED"`,
      type: `string`
    },

    tailNumber: {
      sql: `${CUBE}."TAIL_NUMBER"`,
      type: `string`
    },

    destinationAirport: {
      sql: `${CUBE}."DESTINATION_AIRPORT"`,
      type: `string`
    },

    originAirport: {
      sql: `${CUBE}."ORIGIN_AIRPORT"`,
      type: `string`
    },

    cancellationReason: {
      sql: `${CUBE}."CANCELLATION_REASON"`,
      type: `string`
    }
  }
});
